import React, {Component} from 'react'
import axios from 'axios'
import Posts from "../../components/Posts/Posts";
import Spinner from "../../components/UI/Spinner/Spinner";

class Home extends Component {
	
	state = {
		posts: [],
		loading: false
	};
	
	updatePosts = async () => {
		this.setState({loading: true});
		let posts = await axios.get('/posts.json').finally(() => {
			this.setState({loading: false});
		});
		let newPosts = [];
		for (let key in posts.data) {
			newPosts.push({
				title: posts.data[key].title,
				id: key,
				date: posts.data[key].date,
				text: posts.data[key].text,
				disabled: posts.data[key].disabled
			});
		}
		this.setState({posts: newPosts});
	};
	
	componentDidMount() {
		this.updatePosts();
	}
	
	getFullPost = (id) => {
		this.props.history.push({
			pathname: '/posts/' + id
		})
	};
	
	render() {
		return this.state.loading ? <Spinner/> : <Posts	readMore={this.getFullPost}	posts={this.state.posts}/>;
	}
}

export default Home;